import sys
from random import randint
import math
import numpy
import matplotlib.pyplot as plt
import itertools
import datetime


#############################################################################

def distance(point_a, point_b):
    xa, ya = point_a
    xb, yb = point_b
    return math.sqrt((xa - xb) ** 2 + (ya - yb) ** 2)


def count_final_distance(cities):
    dist = 0
    size = len(cities)
    for i in range(size):
        dist += distance(cities[i], cities[(i + 1) % size])
    return dist


#############################################################################

def probability(x, new_d, best_d, temp):
    return math.exp(- (new_d - best_d) / temp)


def permute(cities):
    random_city = randint(0, len(cities) - 1)
    second_city = randint(0, len(cities) - 1)
    if second_city == random_city:
        random_city = (random_city + 1) % len(cities)
    # print(random_city, second_city)
    cities[random_city], cities[second_city] = cities[second_city], cities[random_city]
    return cities


def simulated_annealing(cities, max_iter):
    start_time = datetime.datetime.now()
    best_distance = count_final_distance(cities)
    best_of_the_best = best_distance
    best_way = cities.copy()
    print(best_way)
    print(best_distance)
    temp = 100000.0
    distance_in_k_steps = []
    for k in range(1, max_iter + 1):
        permuted_cities = permute(best_way)
        # permuted_cities = numpy.random.permutation(cities)
        new_distance = count_final_distance(permuted_cities)
        if new_distance < best_distance:
            best_distance = new_distance
            best_way = permuted_cities.copy()
        elif numpy.random.uniform(0, 1) < probability(k / max_iter, new_distance, best_distance, temp):
            best_distance = new_distance
            best_way = permuted_cities.copy()
        temp = temp * 0.98
        if k % 100 == 0:
            print(best_way)
            print(best_distance)
        distance_in_k_steps.append(best_distance)
        if new_distance < best_of_the_best:
            best_of_the_best = new_distance
    plt.figure(1)
    plt.title('Best distance ' + str(best_of_the_best))
    plt.plot([i for i in range(1, max_iter + 1)], distance_in_k_steps)
    return best_way, best_of_the_best, (datetime.datetime.now() - start_time).total_seconds()


###############################################################################

def display_map(best_way, num, title):
    plt.figure(num)
    plt.title(title)
    xs = [x for [x, _] in best_way] + [best_way[0][0]]
    ys = [y for [_, y] in best_way] + [best_way[0][1]]
    plt.scatter(xs, ys)
    plt.plot(xs, ys)


###############################################################################

def brute_force(cities):
    start_time = datetime.datetime.now()
    all_permutations = list(itertools.permutations(cities))
    best_distance = count_final_distance(all_permutations[0])
    # print(all_permutations)
    best_way = all_permutations[0]
    for permutation in all_permutations:
        new_distance = count_final_distance(permutation)
        if new_distance < best_distance:
            best_distance = new_distance
            best_way = permutation
    return best_way, best_distance, (datetime.datetime.now() - start_time).total_seconds()


###############################################################################

def main(argv):
    number_of_points = 1000
    max_iter = 10000
    cities = [[randint(-17, 17), randint(-17, 17)] for _ in range(number_of_points)]
    plt.figure(0)
    plt.title("Cities")
    plt.plot([x for [x, _] in cities], [y for [_, y] in cities], 'ro')
    plt.axis([-18, 18, -18, 18])
    display_map(cities, 2, "First version")
    best_way, best_distance, sa_time = simulated_annealing(cities, max_iter)
    print("DONE")
    # brute_way, brute_distance, bf_time = brute_force(cities)
    print("RESULTS simulated_annealing")
    print(best_way)
    print(best_distance)
    print("TIME " + str(sa_time))
    display_map(best_way, 3, "Simulated annealing")
    # print("RESULTS brute force")
    # print(brute_way)
    # print(brute_distance)
    # print("TIME " + str(bf_time))
    # display_map(brute_way, 4, "Brute force")
    plt.show()


if __name__ == "__main__":
    main(sys.argv)
