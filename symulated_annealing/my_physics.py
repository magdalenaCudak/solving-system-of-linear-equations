import sys
from random import randint
import math
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
import copy

epsilon = 1e-7


#############################################################################

def distance(point_a, point_b):
    xa, ya = point_a
    xb, yb = point_b
    return math.sqrt((xa - xb) ** 2 + (ya - yb) ** 2)


def mass(rgb):
    [r, g, b] = rgb
    return 3 * (255 - r) + 5 * (255 - g) + 7 * (255 - b)


def count_energy_for_point(this_point, points):
    this_mass = mass(this_point["rgb"])
    sum_force = 0
    for obj in points:
        r = distance((this_point["x"], this_point["y"]), (obj["x"], obj["y"]))
        if r > epsilon:
            m2 = mass(obj["rgb"])
            E = (m2 * this_mass) / r
            sum_force += E
    return sum_force


#############################################################################

def count_total_energy(points):
    energy = 0.0
    for point in points:
        energy += count_energy_for_point(point, points)
    return energy / 10000


##############################################################################

def probability(x, new_d, best_d, temp):
    return math.exp(- (new_d - best_d) / temp)  # math range error


def permute(array, points):
    for _ in range(1, 5): #randint(1, 2)):
        ha = randint(0, len(points) - 1)
        pointA = points[ha]
        xa = pointA["x"]
        ya = pointA["y"]
        a = randint(0, len(array) - 1)
        b = randint(0, len(array[0]) - 1)
        while a == xa and b == ya:
            a = randint(0, len(array) - 1)
        # print('first_permutation', a, b, array[a][b], xa, ya, array[xa][ya])
        tmp = array[a][b].copy()
        array[a][b] = array[xa][ya].copy()
        array[xa][ya] = tmp
        pointA["x"] = a
        pointA["y"] = b
        points[ha] = pointA
        # print('after first_permutation', a, b, array[a][b], xa, ya, array[xa][ya])
    return array, points


def simulated_annealing(array, points, max_iter):
    total_energy = count_total_energy(points)
    print('total energy', total_energy)
    min_of_min = total_energy
    best_result = copy.deepcopy(array)
    # print(best_result)
    # im = Image.fromarray(best_result)
    # im.show(title='Best result')
    temp = 10000.0
    calm_in_k_steps = [total_energy]
    for k in range(1, max_iter + 1):
        permuted_array, permuted_points = permute(best_result, points)
        new_energy = count_total_energy(permuted_points)
        if new_energy < total_energy:
            total_energy = new_energy
            best_result = copy.deepcopy(permuted_array)
        elif np.random.uniform(0, 1) < probability(k / max_iter, new_energy, total_energy, temp):
            total_energy = new_energy
            best_result = copy.deepcopy(permuted_array)
            # print('y')
        temp = temp * 0.98
        if k % 100 == 0:
            im = Image.fromarray(best_result)
            im.save('After_{}.bmp'.format(k))
            print('After {} steps'.format(k))
            print('total energy', total_energy)
        calm_in_k_steps.append(total_energy)
        if new_energy < min_of_min:
            min_of_min = new_energy
    im = Image.fromarray(best_result)
    im.save('Final result.bmp')
    plt.title('Best result ' + str(min_of_min))
    plt.plot([i for i in range(1, max_iter + 2)], calm_in_k_steps)
    figure = plt.gcf()
    figure.savefig("Physics Result.jpg")
    plt.show()
    return total_energy


###############################################################################


def main(argv):
    palette = [[255, 10, 0], [0, 255, 0], [128, 0, 128], [200, 128, 0], [255, 255, 255]]
    width = 100
    height = 200
    image_array = np.zeros([height, width, 3], dtype=np.uint8)
    for i in range(150):
        image_array[randint(0, height - 1)][randint(0, width - 1)] = palette[i % 5]

    color_points = []
    for ix, x in enumerate(image_array):
        for iy, y in enumerate(x):
            # print(y)
            r, g, b = y
            if r == g == b == 0:
                continue
            else:
                next_dict = {
                    "x": ix,
                    "y": iy,
                    "rgb": y
                }
                color_points.append(next_dict)
                # print(next_dict)
    im = Image.fromarray(image_array)
    im.save('First.bmp')
    print('All points number: ', len(color_points))
    max_iter = 1000
    simulated_annealing(image_array, color_points, max_iter)


if __name__ == "__main__":
    main(sys.argv)
