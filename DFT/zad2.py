import sys
import cv2
import numpy as np
from scipy.fftpack import dctn, idctn
from matplotlib import pyplot as plt
import math
import gzip


def main(argv):
    img = cv2.imread('black_hole.jpg', 0)

    quantisation_table = np.matrix([
        [16, 11, 10, 16, 24, 40, 51, 61],
        [12, 12, 14, 19, 26, 58, 60, 55],
        [14, 13, 16, 24, 40, 57, 69, 56],
        [14, 17, 22, 29, 51, 87, 80, 62],
        [18, 22, 37, 56, 68, 109, 103, 77],
        [24, 35, 55, 64, 81, 104, 113, 92],
        [49, 64, 78, 87, 103, 121, 120, 101],
        [72, 92, 95, 98, 112, 100, 103, 99]]
    ).astype('float')

    magnitude_spectrum = np.zeros(img.shape)
    img_transformed = np.zeros(img.shape)
    for i in range(0, int(math.ceil((img.shape[0] - 1) / 8))):
        for j in range(0, int(math.ceil((img.shape[1] - 1) / 8))):
            block = np.copy(img[i * 8: (i + 1) * 8, (j * 8): (j + 1) * 8])
            # for x in range(0, 8):
            #     for y in range(0, 8):
            #         if x < block.shape[0] and y < block.shape[1]:
            #             block[x][y] -= 128
            block = dctn(block)
            block = make_quantisation(block, quantisation_table)
            img_transformed[i * 8: (i + 1) * 8, (j * 8): (j + 1) * 8] = block

    for i in range(0, int(math.ceil((img.shape[0] - 1) / 8))):
        for j in range(0, int(math.ceil((img.shape[1] - 1) / 8))):
            block = img_transformed[i * 8: (i + 1) * 8, (j * 8): (j + 1) * 8]
            block = idctn(block)
            # for x in range(0, 8):
            #     for y in range(0, 8):
            #         if x < block.shape[0] and y < block.shape[1]:
            #             block[x][y] += 128
            magnitude_spectrum[i * 8: (i + 1) * 8, (j * 8): (j + 1) * 8] = block

    compressed = gzip.compress(img_transformed)
    with gzip.open('output.tar.gz', 'wb') as f:
        f.write(compressed)
    plt.subplot(121), plt.imshow(img, cmap='gray')
    plt.title('Input Image'), plt.xticks([]), plt.yticks([])
    plt.subplot(122), plt.imshow(magnitude_spectrum, cmap='gray')
    plt.title('After compression'), plt.xticks([]), plt.yticks([])
    plt.show()


def make_quantisation(block, quantisation_table):
    for x in range(0, 8):
        for y in range(0, 8):
            if x < block.shape[0] and y < block.shape[1]:
                block[x][y] = block.item(x, y) / quantisation_table.item(x % 8, y % 8)
    return block


if __name__ == "__main__":
    main(sys.argv)
