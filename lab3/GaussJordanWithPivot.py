import sys
import numpy

epsilon = 1e-7


def find_pivot(matrix, y, index):
    """
    :param matrix: 2D
    :param y: array
    :param index: currently computed column
    :return: matrix with changed rows
    """
    max_row = matrix[index]
    max_row_index = index
    for i in range(index + 1, len(y)):
        if abs(matrix.item((i, index))) > abs(max_row[index]):
            max_row = matrix[i]
            max_row_index = i
    if max_row_index != index:
        t = matrix[index].copy()
        z = matrix[max_row_index].copy()
        matrix[index] = z
        matrix[max_row_index] = t
    y[index], y[max_row_index] = y[max_row_index], y[index]
    return matrix, y


def eliminate(matrix, y, i):
    """
    :param matrix: 2D
    :param y: array
    :param i: currently computation starts with matrix[i][i]
    :return: Gaussian elimination of single column
    """
    size = len(y)
    for r in range(i + 1, size):
        value = matrix.item((i, i))
        first_in_row = matrix.item((r, i))
        if abs(first_in_row) > epsilon:
            for c in range(i, size):
                matrix.itemset((r, c), (matrix.item((r, c)) * value / first_in_row) - matrix.item((i, c)))
            y[r] = y[r] * value / first_in_row - y[i]


def find_x(matrix, y):
    """
    :param matrix: 2D
    :param y: array
    :return: array x result of: matrix * x = y
    """
    size = len(y)
    result = size * [0]
    for i in range(size):
        index = size - i - 1
        result[index] = y[index]
        for j in range(size - 1, index, -1):
            result[index] = result[index] - matrix.item((index, j)) * result[j]

        if abs(matrix.item((index, index))) > epsilon:
            result[index] = result[index] / matrix.item((index, index))
        else:
            if result[index] == 0.0:
                return "Infinitely many solutions"
            else:
                return "No solution"
    return result


def count_result_uing_pivot(matrix, y):
    """
    :param matrix: 2D
    :param y: array
    :return: Gaussian elimination with pivoting
    """
    for i in range(len(y)):
        matrix, y = find_pivot(matrix, y, i)
        eliminate(matrix, y, i)
    return find_x(matrix, y)


def count_result(matrix, y):
    """
    :param matrix: 2D
    :param y: array
    :return: simply Gaussian elimination
    """
    for i in range(len(y)):
        eliminate(matrix, y, i)
    return find_x(matrix, y)


def solve(matrix, y):
    """
    :param matrix: input matrix
    :param y: input y array
    :return: tuple with results of different solving methods
    """
    m_GJ = matrix.copy()
    y_GJ = y.copy()
    m_GJP = m_GJ.copy()
    y_GJP = y_GJ.copy()
    result_GJ = count_result(m_GJ, y_GJ)
    no_solution = False
    inf_solutions = False
    if result_GJ == "No solution":
        no_solution = True
    elif result_GJ == "Infinitely many solutions":
        inf_solutions = True
    result_with_pivot = count_result_uing_pivot(m_GJP, y_GJP)
    if result_with_pivot == "No solution":
        no_solution = True
    elif result_with_pivot == "Infinitely many solutions":
        inf_solutions = True
    if not no_solution and not inf_solutions:
        result_lib = numpy.linalg.solve(matrix, y)
    else:
        if no_solution:
            return "No solution", [], []
        else:
            return "Infinitely many solutions", [], []
    return result_GJ, result_with_pivot, result_lib


def count_approximation_error(library_result, my_result):
    """
    :param library_result: result of correct computing using library method
    :param my_result: result counted using Gaussian elimination
    :return: pair - absolute error and relative error
    """
    absolute_error = []
    relative_error = []
    for i in range(len(my_result)):
        absolute_error.append(abs(library_result[i] - my_result[i]))
        if abs(library_result[i]) > epsilon:
            relative_error.append(abs((library_result[i] - my_result[i]) / library_result[i]))
        else:
            relative_error.append("nan")
    return absolute_error, relative_error


def count_avg_of_error(approximation_error):
    suma = 0
    for i in range(len(approximation_error)):
        if approximation_error[i] != 'nan':
            suma += approximation_error[i]
    return suma / len(approximation_error)


def print_output(result_GJ, result_GJP, result_lib):
    print("----numpy.linalg.solve----")
    print(numpy.array(result_lib))
    print("\n-------Gauss-Jordan-------")
    print(numpy.array(result_GJ))
    absolute_err, relative_err = count_approximation_error(result_lib, result_GJ)
    print("Absolute error is: ")
    print(numpy.array(absolute_err))
    print("Average absolute error: " + str(count_avg_of_error(absolute_err)))
    print("Relative error is: ")
    print(numpy.array(relative_err))
    print("Average absolute error: " + str(count_avg_of_error(relative_err)))
    print("\n--Gauss-Jordan with pivot--")
    print(numpy.array(result_GJP))
    absolute_err, relative_err = count_approximation_error(result_lib, result_GJP)
    print("Absolute error is: ")
    print(numpy.array(absolute_err))
    print("Average absolute error: " + str(count_avg_of_error(absolute_err)))
    print("Relative error is: ")
    print(numpy.array(relative_err))
    print("Average absolute error: " + str(count_avg_of_error(relative_err)))


def main(argv):
    # matrix = [[5.0, 2.0, 8.0, 5.0],
    #           [5.0, 6.5, 7.0, 6.0],
    #           [3.0, 7.0, 3.0, 6.0],
    #           [5.0, 6.0, 5.0, 6.0]]
    # matrix = [[1.0, 2.0, 8.0, 5.0],
    #           [5.0, 6.0, 7.0, 6.0],
    #           [3.0, 7.0, 3.0, 6.0],
    #           [5.0, 6.0, 5.0, 6.0]]
    matrix1 = [[1.0, 2.0, 8.0, 5.0],
               [5.0, 6.0, 7.0, 6.0],
               [3.0, 7.0, 3.0, 6.0],
               [5.0, 6.0, 5.0, 6.0543210008]]
    matrix = numpy.array([[numpy.random.uniform(-0.01, 0.01) for _ in range(111)] for _ in range(111)])
    # matrix = [[1.0, 2.0],
    #           [1.0, 2.0]]
    # y = [10.0, 10.0]
    # matrix = [[0.02, 0.01, 0, 0],
    #           [1, 2, 1, 0],
    #           [0, 1, 2, 1],
    #           [0, 0, 100, 200]]
    y = numpy.array([numpy.random.uniform(-0.01, 0.01) for _ in range(111)])
    # y = [0.02, 1, 4, 800]
    result_GJ, result_GJP, result_lib = solve(matrix, y)
    if result_GJ == "No solution" or result_GJ == "Infinitely many solutions":
        print(result_GJ)
    else:
        print_output(result_GJ, result_GJP, result_lib)


if __name__ == "__main__":
    main(sys.argv)
